package com.CJMOIO.AnyBeam;
/*
 * AnyBeam - A C. Montgomery & O. Obushenko Product
 * Code is property of C. Montgomery & O. Obushenko
 */
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class IntroductionActivity extends Activity{

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.send_receive);
		
		Button sending_button = (Button)findViewById(R.id.sending_button);
		Button receiving_button = (Button)findViewById(R.id.receiving_button);
		
		sending_button.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivity(new Intent(IntroductionActivity.this,MainActivity.class));
			}
		});
		receiving_button.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
			startActivity(new Intent(IntroductionActivity.this,WaitingActivity.class));
			}
		});
	}
	
	
}
