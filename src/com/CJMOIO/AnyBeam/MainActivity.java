package com.CJMOIO.AnyBeam;
/*
 * AnyBeam - A C. Montgomery & O. Obushenko Product
 * Code is property of C. Montgomery & O. Obushenko
 */
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import android.os.Bundle;
import android.os.Environment;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

public class MainActivity extends Activity {
	String path;
	String fileName;
	public static String FILE_PATH = "file_path";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		String whatToDo="Use the panes to select a file to transfer. Directories are up top, Files are on the bottom";
		Toast.makeText(getApplicationContext(), whatToDo, Toast.LENGTH_LONG).show();

		
		path=Environment.getExternalStorageDirectory()+File.separator;
		makeDirectoryList(path);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	public void makeDirectoryList(String directory){
		final ListView myList=(ListView)findViewById(R.id.dirArea);
		// Creates a file of the current directory
		File dirManager = new File(directory);
		// Populate an array list with the names of the files within the directory
		ArrayList<String> numberOfDirs=new ArrayList<String>(Arrays.asList(dirManager.list()));
		// Creates an array list that will contain the names of the directories within the directory
		ArrayList<String> listDirectories=new ArrayList<String>();
		//ArrayAdapter<String> adaptDirs=new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,listDirectories);
		ArrayAdapter<String> adaptDirs=new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,listDirectories);
		// Binds the directory list to the array adapter
		myList.setAdapter(adaptDirs);
		myList.setSelector(R.color.gray);
		String dirName;
		
		File [] toAdd=dirManager.listFiles();
		
		dirName="/                    ";
		listDirectories.add(dirName);
		dirName="../                  ";
		listDirectories.add(dirName);
		
		/*
		 * Goes through the directories contents to determine which items
		 * are directories and adds them to the listDirectories ArrayList
		 * which is binded to the ListView via the ArrayAdapter adaptDirs.
		 */
		int i=0;
		while(i<(numberOfDirs.size())){
			boolean isDir;
			
			isDir=toAdd[i].isDirectory();
			if(isDir==true){
				// Takes the directory name and adds a '/' after it to denote directory status
				dirName=toAdd[i].getName()+ "/";
				listDirectories.add(dirName);
			}
			adaptDirs.notifyDataSetChanged();
			
			i++;
		}
		/*
		 *  Call the makeListofFiles method to populate the bottom pane with the 
		 *  Radio Group of files within the directory 'path'
		 */
		makeListofFiles(path);
		// Make the ListView clickable
		myList.setClickable(true);
		// Define the onClick functionality for each directory in the listView
		myList.setOnItemClickListener(new android.widget.AdapterView.OnItemClickListener() {
		 

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) 
			{
				//
					File thisPath=new File(path);
					arg0=myList;
					String get=(String) arg0.getItemAtPosition(arg2);
					if(get=="../                  "){
						// This may need to be modified to avoid reaching
						// root directories
						if(!thisPath.equals(Environment.getExternalStorageDirectory()))
						{
							path=thisPath.getParent()+File.separator;
						}
						else{
							Toast.makeText(getApplicationContext(), "Insufficient privileges to access higher directory", Toast.LENGTH_SHORT).show();
							path = Environment.getExternalStorageDirectory()+File.separator;
						}
					}
		       
					else if(get=="/                    "){
						path=Environment.getExternalStorageDirectory()+File.separator;
					}
					else{
						path=path+get;
					}   
					Context something=MainActivity.this;
					CharSequence toastWords=path;
					//Toast thisDirectory=Toast.makeText(something,toastWords,Toast.LENGTH_SHORT);
					//thisDirectory.show();
					makeDirectoryList(path);
		       }
	    });
	}
		
	
	public void makeListofFiles(String directory){
		final RadioGroup myRadioGroup=(RadioGroup) findViewById(R.id.fileArea);
		// Creates a file of the current directory
		File manager =new File(directory);
		// populate an array list with the names of the files within the directory
		ArrayList<String> listFiles=new ArrayList<String>(Arrays.asList(manager.list()));
		File [] setofFiles=manager.listFiles();
		// clear the previous radiogroup view of previous files since we changed directories
		myRadioGroup.removeAllViews();
		
		
		int n=0;
		int o=listFiles.size();
		int p=0;
		/*
		 * Creates a new radio button for each file (not directories) within the
		 * current directory 'directory'. 
		 */
		while(n<o){
			
			RadioButton myRadioButton=new RadioButton(this);
			myRadioButton.setTextSize(20);
			myRadioButton.setTextColor((getResources().getColor(android.R.color.black)));
			//myRadioButton.setButtonDrawable(R.drawable.radio_gray);
			myRadioButton.setPadding(10,30,10,30);
			if (!setofFiles[n].isDirectory()){
				myRadioGroup.addView(myRadioButton, p);
				myRadioButton.setText("      "+listFiles.get(n).toString());
				// Define the absolute file path of the current file
				final String fileabsolutepath = directory + listFiles.get(n);
				/*
				 *  Creates an onClickListener for the file that will launch
				 *  our filetransfer activity and pass a string of the
				 *  absolute file path of the file to be sent, if it is selected
				 *  from the Radio Group
				 */
				myRadioButton.setOnClickListener(new View.OnClickListener() {
					public void onClick(View v) {
						Intent passtofiletransfer = new Intent(MainActivity.this, TransferSelectionActivity.class);
						passtofiletransfer.putExtra(FILE_PATH,fileabsolutepath);
						startActivity(passtofiletransfer);
					}
				});
				p++;
			}
			n++;
		}
	
	}
	
	
}
