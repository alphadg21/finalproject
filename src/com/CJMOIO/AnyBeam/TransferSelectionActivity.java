package com.CJMOIO.AnyBeam;
/*
 * AnyBeam - A C. Montgomery & O. Obushenko Product
 * Code is property of C. Montgomery & O. Obushenko
 */
import java.io.File;
import java.util.List;

import android.annotation.TargetApi;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Color;
import android.net.Uri;
import android.net.wifi.p2p.WifiP2pManager;
import android.net.wifi.p2p.WifiP2pManager.Channel;
//import android.net.wifi.p2p.WifiP2pManager;
//import android.net.wifi.p2p.WifiP2pManager.Channel;
import android.nfc.NfcAdapter;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class TransferSelectionActivity extends Activity {

	private BluetoothAdapter bluetooth = null;
	private NfcAdapter myNfcAdapter = null;
	private WifiP2pManager wifiP2pManager = null;
	private Channel wifiDirectChannel = null;
	protected static final int DISCOVERY_REQUEST = Menu.FIRST+2;
	protected static final String TAG = "BLUETOOTH";

    public static String EXTRA_DEVICE_ADDRESS = "device_address";
    File file_to_transfer;
    
	@SuppressWarnings("deprecation")
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//getWindow().setFormat(PixelFormat.RGB_565);
		setContentView(R.layout.transfer_selection);
		
		TextView main_text_block = (TextView)findViewById(R.id.textView1);
		Button button_for_nfc = (Button)findViewById(R.id.nfcbutton);
		Button button_for_bluetooth = (Button)findViewById(R.id.bluetoothbutton);
		Button button_for_wifi = (Button)findViewById(R.id.wifibutton);
		Button button_go_back = (Button)findViewById(R.id.gobackbutton);
		
		String data = getIntent().getStringExtra("file_path");
		main_text_block.setText("File to be sent is: \n" +data+ "\n \nWhat method of transfer would you like to use?");
		main_text_block.setTextSize(20); 
		file_to_transfer = new File(data);
		//Toast.makeText(getApplicationContext(), file_to_transfer.toString(), Toast.LENGTH_SHORT).show();
		if(NfcAdapter.getDefaultAdapter(getBaseContext()) == null)
		{
			button_for_nfc.setBackgroundDrawable(getResources().getDrawable(R.drawable.button_gray_and_x));
		}
		else
			button_for_nfc.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				// Still needs modification
				transferviaNFC();
			}
			});
		if(BluetoothAdapter.getDefaultAdapter() == null)
		{
			button_for_bluetooth.setBackgroundDrawable(getResources().getDrawable(R.drawable.button_gray_and_x));
		}
		else
			button_for_bluetooth.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				// Pretty much works
				transferviaBT();		
			}
			});
		if(getSystemService(Context.WIFI_P2P_SERVICE) == null)
		{
			button_for_wifi.setBackgroundDrawable(getResources().getDrawable(R.drawable.button_gray_and_x));
		}
		else
		button_for_wifi.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				// Still need to code
				transferviaWIFI();
			}
		});
		
		
		button_go_back.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				startActivity(new Intent(TransferSelectionActivity.this,MainActivity.class));	
			}
		});

	}
	/*
	 * 
	 * 
	 * 
	 */
	
	
	
// Define other code below	
	@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
	public void transferviaNFC()
	{
		 myNfcAdapter = NfcAdapter.getDefaultAdapter(getBaseContext());
		 if(myNfcAdapter != null)
		 {
			 if(myNfcAdapter.isEnabled())
			 {
			 Uri toPush=Uri.fromFile(file_to_transfer);
			 Toast.makeText(getBaseContext(),(String)toPush.toString(), Toast.LENGTH_SHORT).show();
			 myNfcAdapter.setBeamPushUris(new Uri [] {toPush},this);
			 }
			 else
			 {
				 Toast.makeText(getBaseContext(), "NFC Adapter not enabled, please make sure NFC is enabled", Toast.LENGTH_SHORT).show();
			 }
		 }
		 else{
			 Toast.makeText(getBaseContext(), "NFC Adapter not found, please make sure NFC is enabled", Toast.LENGTH_SHORT).show();
			 finish();
		 }
		 
	}
	
	public void transferviaBT()
	{
		bluetooth = BluetoothAdapter.getDefaultAdapter();
		bluetooth.enable();
		Intent intenttosend = new Intent();
		intenttosend.setAction(Intent.ACTION_SEND);
		intenttosend.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file_to_transfer));
		intenttosend.setType("*/*");
		PackageManager pm = getPackageManager();
		List<ResolveInfo> appsList = pm.queryIntentActivities(intenttosend, 0);
		if(appsList.size() > 0) {
			   // proceed
			String packageName = null;
			String className = null;
			boolean found = false;
			for(ResolveInfo info: appsList){
			  packageName = info.activityInfo.packageName;
			  if( packageName.equals("com.android.bluetooth")){
			     className = info.activityInfo.name;
			     found = true;
			     break;// found
			  }
			}
			if(! found){
			  Toast.makeText(this, "Bluetooth Application Not Found",
			                 Toast.LENGTH_SHORT).show();
			  finish();
			}
				intenttosend.setClassName(packageName, className);
				startActivity(intenttosend);
		}
		
	}

	public void transferviaWIFI()
	{
		
	}

}
